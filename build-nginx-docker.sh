#!/bin/bash

export SCRIPT_DIR="$(cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPT_DIR


IMAGE_NAME=ahmadshams/exam:nginx-app

if [ -z $BUILD_NUMBER ]; then
	BUILD_NUMBER=$(shuf -i 1-100 -n 1)
fi

cp -f index.html-orig index.html
perl -pi -e "s/<build_number>/$BUILD_NUMBER/;" index.html


docker build -f Dockerfile -t  $IMAGE_NAME .
docker push $IMAGE_NAME






